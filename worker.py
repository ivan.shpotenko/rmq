#!/usr/bin/env python
import json
from time import sleep

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')


def callback(ch, method, properties, body='hello'):
    body = json.loads(body)
    print(" [x] Received %r" % body)
    sleep(5)
    channel.queue_declare(queue=body['callback_queue'])

    channel.basic_publish(exchange='',
                          routing_key=body['callback_queue'],
                          body=body['body'])
    print(f" [x] Sent '{body['body']}'")


channel.basic_consume(callback,
                      queue='hello',
                      no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
