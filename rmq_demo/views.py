import json

from rest_framework.response import Response
from rest_framework.views import APIView
import pika

INPUT_QUEUE = 'hello'


class DemoView(APIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    def get(self, request, body):
        callback_queue = 'custom_uid_queue'
        msg = {'body': body,
               'callback_queue': callback_queue}
        msg = json.dumps(msg)
        self.send_msg(msg)
        message = self.get_msg(callback_queue=callback_queue)

        print(f'hey {message}')
        data = {'hello': f'{message}'}
        return Response(data)

    def get_msg(self, callback_queue):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()
        channel.queue_declare(callback_queue)
        for method, properties, body in channel.consume(callback_queue):
            if method:
                print(method, body)
                channel.basic_ack(method.delivery_tag)
                channel.cancel()
                channel.queue_delete()
                connection.close()
                return body

    def send_msg(self, msg):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()
        channel.queue_declare(queue=INPUT_QUEUE)
        channel.basic_publish(exchange='',
                              routing_key=INPUT_QUEUE,
                              body=msg)
        print(f" [x] Sent '{msg}'")
        connection.close()
